import { Component, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

@Injectable()
export class HomePage{

  constructor(public router: Router, public alertCtrl: AlertController, public nav: NavController) {}

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Ingresar Contraseña',
      // subHeader: 'Important message',
      // message: 'This is an alert!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'Cancel'
        },
        {
          text: 'OK',
          handler: data =>{
              if(data.passw === "UtvcoS789"){
                this.nav.navigateForward('/utvco/UtvcoS789')
              }else{
                window.alert('Contraseña incorrecta');
              }
          }
        }
      ],
      inputs: [
        {
          name:'passw',
          type: 'password',
          placeholder: 'Contraseña'
        }
      ]
    });

    await alert.present();
  }


}