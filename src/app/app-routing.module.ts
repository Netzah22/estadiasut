import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { UtvcoPage } from './pages/utvco/utvco.page';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'formulario',
    loadChildren: () => import('./pages/formulario/formulario.module').then( m => m.FormularioPageModule)
  },
  {
    path: 'carreras',
    loadChildren: () => import('./pages/carreras/carreras.module').then( m => m.CarrerasPageModule)
  },
  {
    path: 'gastro',
    loadChildren: () => import('./pages/gastro/gastro.module').then( m => m.GastroPageModule)
  },
  {
    path: 'des',
    loadChildren: () => import('./pages/des/des.module').then( m => m.DesPageModule)
  },
  {
    path: 'utvco/:passw',
    loadChildren: () => import('./pages/utvco/utvco.module').then( m => m.UtvcoPageModule)
  },
  {
    path: 'meca',
    loadChildren: () => import('./pages/meca/meca.module').then( m => m.MecaPageModule)
  },
  {
    path: 'ti',
    loadChildren: () => import('./pages/ti/ti.module').then( m => m.TiPageModule)
  },
  {
    path: 'energias',
    loadChildren: () => import('./pages/energias/energias.module').then( m => m.EnergiasPageModule)
  },
  {
    path: 'moda',
    loadChildren: () => import('./pages/moda/moda.module').then( m => m.ModaPageModule)
  },
  {
    path: 'agricultura',
    loadChildren: () => import('./pages/agricultura/agricultura.module').then( m => m.AgriculturaPageModule)
  },
  {
    path: 'procesos',
    loadChildren: () => import('./pages/procesos/procesos.module').then( m => m.ProcesosPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
