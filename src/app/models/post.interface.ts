export interface AddBuss{
    correo: string,
    empresa: string,
    contacto: string,
    puesto: string,
    telefono: string,
    correoCon: string,
    direccion: string,
    proyecto: string,
    perfil: string,
    apoyo: string,
    comentarios: string
}