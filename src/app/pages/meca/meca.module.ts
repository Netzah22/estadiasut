import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MecaPageRoutingModule } from './meca-routing.module';

import { MecaPage } from './meca.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MecaPageRoutingModule
  ],
  declarations: [MecaPage]
})
export class MecaPageModule {}
