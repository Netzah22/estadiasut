import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';

@Component({
  selector: 'app-meca',
  templateUrl: './meca.page.html',
  styleUrls: ['./meca.page.scss'],
})
export class MecaPage implements OnInit {

  meca: any;
  mecaIng: any;

  constructor(private http: InfoService) { }

  ngOnInit() {
    this.obtenerMeca();
    this.obtenerMecaING();
  }

  obtenerMeca(){
    this.http.getMeca().subscribe( (data:any)=>{
      this.meca = data.data;
    })
  }

  obtenerMecaING(){
    this.http.getMecaIng().subscribe( (data:any)=>{
      this.mecaIng = data.data;
    })
  }

}
