import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';

@Component({
  selector: 'app-carreras',
  templateUrl: './carreras.page.html',
  styleUrls: ['./carreras.page.scss'],
})
export class CarrerasPage implements OnInit {

  gaTsu;
  gaIng;
  conteoIngGa;

  deTsu;
  deIng;
  conteoDe;

  teTsu;

  meTsu;
  meIng;
  conteoMe;

  moTsu;
  moIng;
  conteoMo;

  agrTsu;

  eneTsu;

  constructor(private infoS: InfoService) { }

  ngOnInit() {
    this.gTsu();
    this.gIng();
    //
    this.dTsu();
    this.dIng();
    //
    this.tTsu();
    //
    this.mTsu();
    this.mIng();
    //
    this.modTsu();
    this.modIng();
    //
    this.agTsu();
    //
    this.enTsu();
  }

  gTsu(){
    this.infoS.gTsu().subscribe( (data:any)=>{
      this.gaTsu = data.data[0]["COUNT(*)"];
    })
  }
  gIng(){
    this.infoS.gIng().subscribe( (data:any)=>{
      this.gaIng = data.data[0]["COUNT(*)"];
      this.conteoIngGa = this.gaIng + this.gaTsu;
    })
  }

  /////////////////////

  dTsu(){
    this.infoS.dTsu().subscribe( (data:any)=>{
      this.deTsu = data.data[0]["COUNT(*)"]; 
    })
  }
  dIng(){
    this.infoS.dIng().subscribe( (data:any)=>{
      this.deIng = data.data[0]["COUNT(*)"];
      this.conteoDe = this.deTsu + this.deIng;
    })
  }

  /////////////////////

  tTsu(){
    this.infoS.tTsu().subscribe( (data:any)=>{
      this.teTsu = data.data[0]["COUNT(*)"];
    })
  }

  /////////////////////

  mTsu(){
    this.infoS.mTsu().subscribe( (data:any)=>{
      this.meTsu = data.data[0]["COUNT(*)"];
    })
  }
  mIng(){
    this.infoS.mIng().subscribe( (data:any)=>{
      this.meIng = data.data[0]["COUNT(*)"];
      this.conteoMe = this.meTsu + this.meIng;
    })
  }

  /////////////////////

  modTsu(){
    this.infoS.moTsu().subscribe( (data:any)=>{
      this.moTsu = data.data[0]["COUNT(*)"];
    })
  }
  modIng(){
    this.infoS.moIng().subscribe( (data:any)=>{
      this.moIng = data.data[0]["COUNT(*)"];
      this.conteoMo = this.moTsu + this.meIng;
    })
  }

  /////////////////////

  agTsu(){
    this.infoS.agTsu().subscribe( (data:any)=>{
      this.agrTsu = data.data[0]["COUNT(*)"];
    })
  }

  /////////////////////

  enTsu(){
    this.infoS.enTsu().subscribe( (data:any)=>{
      this.eneTsu = data.data[0]["COUNT(*)"];
    })
  }

}
