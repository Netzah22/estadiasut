import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';

@Component({
  selector: 'app-des',
  templateUrl: './des.page.html',
  styleUrls: ['./des.page.scss'],
})
export class DesPage implements OnInit {

  des: any;
  desIng: any;

  constructor(private http: InfoService) { }

  ngOnInit() {
    this.getDes();
    this.getDesIng();
  }

  getDes(){
    this.http.getDes().subscribe( (data:any)=>{
      this.des = data.data;
    });
  }

  getDesIng(){
    this.http.getDesIng().subscribe( (data:any)=>{
      this.desIng = data.data;
    });
  }

}
