import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';

@Component({
  selector: 'app-energias',
  templateUrl: './energias.page.html',
  styleUrls: ['./energias.page.scss'],
})
export class EnergiasPage implements OnInit {

  energ: any;

  constructor(private http: InfoService) { }

  ngOnInit() {
    this.obtenerEner();
  }

  obtenerEner(){
    this.http.getEnergias().subscribe( (data:any)=>{
      this.energ = data.data;
    })
  }

}
