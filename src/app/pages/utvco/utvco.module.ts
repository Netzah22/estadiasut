import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UtvcoPageRoutingModule } from './utvco-routing.module';

import { UtvcoPage } from './utvco.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UtvcoPageRoutingModule
  ],
  declarations: [UtvcoPage]
})
export class UtvcoPageModule {}
