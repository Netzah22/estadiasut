import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';
import * as XLSX from 'xlsx';
import { ActivatedRoute, CanActivate } from '@angular/router';

@Component({
  selector: 'app-utvco',
  templateUrl: './utvco.page.html',
  styleUrls: ['./utvco.page.scss'],
})
export class UtvcoPage implements OnInit {
  empresas:any;

  constructor(private http: InfoService, private route: ActivatedRoute) { }

//   canActivate() {
    
//     if(passw === "UtvcoS789"){
//       console.log('entrando...');
//       return true;      
//     }else{
//       console.log('no se puede entrar');
//       console.log(passw);
//       return false;
//     }
// }

  ngOnInit() {
    let passw = this.route.snapshot.paramMap.get('passw');
    this.obtener();
  }

  obtener(){
    this.http.obtener().subscribe( (data:any) =>{
      this.empresas = data.data;
    });
  }

  nombre = 'EmpresasSheet.xlsx';
  exportExcel():void{
    let element = document.getElementById('season-table');
    const worksheet: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    const book: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(book, worksheet, 'Sheet1');

    XLSX.writeFile(book, this.nombre);
  }

}
