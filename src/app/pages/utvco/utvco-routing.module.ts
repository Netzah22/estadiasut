import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UtvcoPage } from './utvco.page';

const routes: Routes = [
  {
    path: '',
    component: UtvcoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UtvcoPageRoutingModule {}
