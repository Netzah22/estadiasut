import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { InfoService } from '../../services/info.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.page.html',
  styleUrls: ['./formulario.page.scss'],
})
export class FormularioPage implements OnInit {

  formulario: FormGroup;

  constructor(private getS: InfoService, public alertCtrl: AlertController, private router: Router) { }

  ngOnInit() {
    this.createForm();
  }

  createForm(){
    this.formulario = new FormGroup({
      correo: new FormControl('', [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'), Validators.required]),
      empresa: new FormControl('', Validators.required),
      contacto: new FormControl('', Validators.required),
      puesto: new FormControl('', Validators.required),
      telefono: new FormControl('', Validators.required),
      correoCon: new FormControl('', [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'), Validators.required]),
      direccion: new FormControl('', Validators.required),
      proyecto: new FormControl('', Validators.required),
      tsu: new FormControl(''),
      ing: new FormControl(''),
      perfil: new FormControl('', Validators.required),
      apoyo: new FormControl('', Validators.required),
      comentarios: new FormControl('', Validators.required)
    })
  }

  enviar(){
    this.getS.envio(this.formulario.value).subscribe(
      (data:any)=>{
      }
    )

    this.presentAlert();
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: '¡Se ha registrado su respuesta!',
      subHeader:'¿Desea registrar otra propuesta?',
      message: 'Presione la opción deseada',
      buttons: [
        {
          text: 'Salir',
          handler: ()=>{
            this.router.navigateByUrl('/home');
          }
        },
        {
          text: 'Agregar',
          handler: ()=>{
            this.formulario.reset();
          }
        }
      ]
    });

    await alert.present();
    
  }

  limpiarForm(){
    this.formulario.reset();
    // location.hash = '#' + id;
  }

}
