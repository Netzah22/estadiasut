import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';

@Component({
  selector: 'app-ti',
  templateUrl: './ti.page.html',
  styleUrls: ['./ti.page.scss'],
})
export class TiPage implements OnInit {

  ti:any;

  constructor(private http: InfoService) { }

  ngOnInit() {
    this.obtenerTi();
  }

  obtenerTi(){
    this.http.getTi().subscribe( (data:any)=>{
      this.ti = data.data;
    })
  }

}
