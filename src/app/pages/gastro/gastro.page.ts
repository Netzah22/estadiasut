import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-gastro',
  templateUrl: './gastro.page.html',
  styleUrls: ['./gastro.page.scss'],
})
export class GastroPage implements OnInit {

  gastsu:any;
  gasing:any;

  constructor(private http: InfoService) { }

  ngOnInit() {
    this.obtenerG();
    this.obtenerGING();
  }

  obtenerG(){
    this.http.getGas().subscribe( (data:any)=>{
      this.gastsu = data.data;
    })
  }

  obtenerGING(){
    this.http.getGasIng().subscribe( (data:any)=>{
      this.gasing = data.data;
    })
  }

}
