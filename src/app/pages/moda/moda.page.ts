import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';

@Component({
  selector: 'app-moda',
  templateUrl: './moda.page.html',
  styleUrls: ['./moda.page.scss'],
})
export class ModaPage implements OnInit {

  moda: any;
  modaIng: any;

  constructor(private http: InfoService) { }

  ngOnInit() {
    this.obtenerModa();
    this.obtenerModaING();
  }

  obtenerModa(){
    this.http.getModa().subscribe( (data:any)=>{
      this.moda = data.data;
    })
  }

  obtenerModaING(){
    this.http.getModaIng().subscribe( (data:any)=>{
      this.modaIng = data.data;
    })
  }

}
