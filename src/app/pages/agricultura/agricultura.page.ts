import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';

@Component({
  selector: 'app-agricultura',
  templateUrl: './agricultura.page.html',
  styleUrls: ['./agricultura.page.scss'],
})
export class AgriculturaPage implements OnInit {

  a: any;
  aIng: any;

  constructor(private http: InfoService) { }

  ngOnInit() {
    this.obtenerA();
  }

  obtenerA(){
    this.http.getAgri().subscribe( (data:any)=>{
      this.a = data.data;
    })
  }

}
