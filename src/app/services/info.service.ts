import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AddBuss } from '../models/post.interface';

@Injectable({
  providedIn: 'root'
})
export class InfoService {

  constructor(private http: HttpClient) { }

  URL="http://localhost:3000/"

  envio(addBuss: AddBuss){
    return this.http.post(`${this.URL}servuni/registro`, addBuss);
  }

  obtener(){
    return this.http.get(`${this.URL}servuni/empresas/list`);
  }

  getGas(){
    return this.http.get(`${this.URL}servuni/empresas/gastro`);
  }

  getGasIng(){
    return this.http.get(`${this.URL}servuni/empresas/gastroI`);
  }

  getDes(){
    return this.http.get(`${this.URL}servuni/empresas/des`);
  }

  getDesIng(){
    return this.http.get(`${this.URL}servuni/empresas/desI`);
  }

  getMeca(){
    return this.http.get(`${this.URL}servuni/empresas/meca`);
  }

  getMecaIng(){
    return this.http.get(`${this.URL}servuni/empresas/mecaI`);
  }

  getTi(){
    return this.http.get(`${this.URL}servuni/empresas/ti`);
  }

  getEnergias(){
    return this.http.get(`${this.URL}servuni/empresas/energias`);
  }

  getModa(){
    return this.http.get(`${this.URL}servuni/empresas/moda`);
  }

  getModaIng(){
    return this.http.get(`${this.URL}servuni/empresas/modaI`);
  }

  getAgri(){
    return this.http.get(`${this.URL}servuni/empresas/agri`);
  }

  getProc(){
    return this.http.get(`${this.URL}servuni/empresas/procesos`);
  }


  // OBTENCION NUMEROS REGISTRO
  gTsu(){
    return this.http.get(`${this.URL}servuni/empresas/gTsu`);
  }
  gIng(){
    return this.http.get(`${this.URL}servuni/empresas/gIng`);
  }

  //DESARROLLO
  dTsu(){
    return this.http.get(`${this.URL}servuni/empresas/dTsu`);
  }
  dIng(){
    return this.http.get(`${this.URL}servuni/empresas/dIng`);
  }

  //TI
  tTsu(){
    return this.http.get(`${this.URL}servuni/empresas/tTsu`);
  }

  //MECA
  mTsu(){
    return this.http.get(`${this.URL}servuni/empresas/mTsu`);
  }
  mIng(){
    return this.http.get(`${this.URL}servuni/empresas/mIng`);
  }

  //MODA
  moTsu(){
    return this.http.get(`${this.URL}servuni/empresas/mTsu`);
  }
  moIng(){
    return this.http.get(`${this.URL}servuni/empresas/mIng`);
  }

  //AGRICULTURA
  agTsu(){
    return this.http.get(`${this.URL}servuni/empresas/agTsu`);
  }

  //ENERGIAS
  enTsu(){
    return this.http.get(`${this.URL}servuni/empresas/enTsu`);
  }
}
